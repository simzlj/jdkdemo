package com.xy6.jvm.fork;

import java.util.List;

/**
 * 计算一组数的和
 * 
 * @author dell
 *
 */
public class SumAbstractService {

	/**
	 * 计算n组数的和
	 * 
	 * @param nums
	 */
	public void sum(List<List<Integer>> nums){
	}

	public int sumNum(List<Integer> nums){
		int sum = 0;
		for (int i = 0; i < nums.size(); i++) {
			sum += nums.get(i);
		}
		return sum;
	}
	
	public void save(int num){
		System.out.println("save: " + num);
	}
}
