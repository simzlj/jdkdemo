package com.xy6.jvm.fork.thread;

import java.util.Random;

public class ProducterThread extends Thread {

	private Random r = new Random();
	
	private String name;
	
	public ProducterThread(String name){
		this.name = name;
	}
	
	@Override
	public void run() {
		while(true){
			int data = r.nextInt(1000);
			Message.push(data);
			System.out.println(String.format("%s data: %d", name, data));
			
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
				break;
			}
		}
	}

	@Override
	public synchronized void start() {
//		System.out.println(String.format("%s begin start", name));
		super.start();
	}
	
}
