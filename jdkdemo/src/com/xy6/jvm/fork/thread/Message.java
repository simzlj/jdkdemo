package com.xy6.jvm.fork.thread;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * 消息队列
 * 
 * @author zhang
 *
 */
public class Message {

	private static BlockingQueue<Integer> messages = new ArrayBlockingQueue<>(10000);
	
	/**
	 * 弹出元素
	 * 
	 * @return
	 */
	public static Integer pop(){
		return messages.poll();
	}
	
	/**
	 * 添加元素
	 * 
	 * @return
	 */
	public static void push(Integer i){
		try {
			messages.put(i);
		} catch (InterruptedException e) {
			System.out.println(String.format("thread interrupt, data not saved: %d", i));
		}
	}
	
}
