package com.xy6.jvm.fork.thread;

public class CustomerThread extends Thread {

	private String name;
	
	public CustomerThread(String name){
		this.name = name;
	}
	
	@Override
	public void run() {
		while(true){
			Integer data = Message.pop();
			if(null != data){
				System.out.println(String.format("%s data: %d", name, data));
			} else {
				System.out.println(String.format("%s data: null", name));
			}
			
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
				break;
			}
		}
	}

}
