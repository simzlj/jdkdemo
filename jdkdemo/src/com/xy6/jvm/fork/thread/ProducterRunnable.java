package com.xy6.jvm.fork.thread;

public class ProducterRunnable implements Runnable {

	private String name;
	
	public ProducterRunnable(String name){
		this.name = name;
	}
	
	@Override
	public void run() {
		System.out.println(String.format("%s begin run", name));
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(String.format("%s end run", name));
	}

}
