package com.xy6.jvm.fork.thread;

import java.io.IOException;

import org.junit.Test;

public class ProducterClient {

	public static void main(String[] args) {
	}

	@Test
	public void testThread1() throws IOException {
		int n = 10;
		for (int i = 0; i < n; i++) {
			Thread t = new ProducterThread("t-" + i);
			t.start();
		}
		System.in.read();
	}

	@Test
	public void testThread2() throws IOException {
		int n = 10;
		for (int i = 0; i < n; i++) {
			Thread t = new Thread(new ProducterRunnable("t-" + i));
			t.start();
		}
		System.in.read();
	}

	@Test
	public void testThread3() throws IOException {
		int m = 5;
		for (int i = 0; i < m; i++) {
			Thread tp = new ProducterThread("product-" + i);
			tp.start();
		}
		int n = 5;
		for (int i = 0; i < n; i++) {
			Thread tc = new CustomerThread(" custom-" + i);
			tc.start();
		}
		System.in.read();
	}

}
