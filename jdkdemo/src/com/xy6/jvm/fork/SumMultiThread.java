package com.xy6.jvm.fork;

import java.util.ArrayList;
import java.util.List;

public class SumMultiThread extends SumAbstractService {

	@Override
	public void sum(List<List<Integer>> nums) {
		// 将n个元素分为m组，m=cpu个数
		int n = nums.size();
		int m = 4;
		int groupSize = Double.valueOf(Math.ceil(n * 1.0 / 4)).intValue();

		List<List<List<Integer>>> groups = new ArrayList<>(m);
		for (int i = 0; i < m; i++) {
			List<List<Integer>> group = new ArrayList<>(groupSize);
			groups.add(group);
		}
		for (int i = 0; i < n; i++) {
			groups.get(i / 3).add(nums.get(i));
		}
		for (int i = 0; i < m; i++) {
			String name = "thread-" + i;
			Thread t = new Thread(new SumThread1(name, groups.get(i)), name);
			t.run();
		}
	}

}
