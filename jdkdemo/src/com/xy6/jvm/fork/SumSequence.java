package com.xy6.jvm.fork;

import java.util.List;

/**
 * 遍历计算n组数的和
 * 
 * @author dell
 *
 */
public class SumSequence extends SumAbstractService {

	@Override
	public void sum(List<List<Integer>> nums){
		for (int i = 0; i < nums.size(); i++) {
			save(sumNum(nums.get(i)));
		}
	}
	
}
