package com.xy6.jvm.fork;

import java.util.List;

/**
 * 多线程demo，计算一组数的和
 * 
 * @author frank
 * @since 2018-01-01
 */
public class SumThread1 implements Runnable {

	/**
	 * 一组数字
	 */
	private List<List<Integer>> nums;
	
	/**
	 * 线程名称
	 */
	private String name;
	
	public SumThread1(String name, List<List<Integer>> nums){
		this.name = name;
		this.nums = nums;
	}

	@Override
	public void run() {
		System.out.println(String.format("%s begin run", this.name));
		for (int i = 0; i < nums.size(); i++) {
			save(sumNum(nums.get(i)));
		}
		System.out.println(String.format("%s end run", this.name));
	}
	
	public int sumNum(List<Integer> nums){
		int sum = 1;
		for (int i = 0; i < nums.size(); i++) {
			sum += nums.get(i);
		}
		return sum;
	}
	
	private void save(int num){
		System.out.println("save: " + num);
	}
	
}
