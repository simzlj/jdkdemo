package com.xy6.jvm.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * BufferWriter操作示例
 * <pre>
 * 为了防止多次操作IO（操作IO很费cpu时间），提供了一个缓冲区，当缓冲区满的时候，再写入文件，从而提高效率。
 * 因此，如果缓冲区没有写满，那么就必须强制他输出到文件，即调用flush();
 * 
 * flush()方法：冲走。意思是把缓冲区的内容强制地写出。
 * 由于操作系统的一些机制，为了防止一直不停地磁盘读写，所以有了延迟读写的概念。
 * 在网络web服务器上也是，为了防止写一个字节就发送一个消息，所以就有了缓冲区的概念，比如64K的内存区域，
 * 缓冲区写满了再一次性写入磁盘之中（或者发送给客户端浏览器）。
 * flush方法一般是程序写入完成时执行，随后跟着close方法。
 * 
 * 本质为批量处理的思想，hbase中MemStore设计、新增大量数据时的批量提交也是采用这一原则。
 * </pre>
 * 
 * @author zhang
 * @since 2018-04-22
 */
public class TestBufferStream {
	
	public static void main(String[] args) {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter("e:\\dat2.txt"));
			BufferedReader br = new BufferedReader(new FileReader("e:\\dat2.txt"));
			
			String s = null;
			for (int i = 1; i <= 5; i++) {
				s = String.valueOf(Math.random());
				bw.write(s);
				bw.newLine();
			}
			/*
			 * 将该行注释，文件仍可以正常生成且有数据，但下面read时无内容输出。
			 * 原因：内容只是写到了缓冲区里，并没有写入文件，所以读取不到内容。
			 * 
			 * 程序结束后为什么文件里面又有内容呢？
			 * 因为你在后面调用close()方法了，这个方法调用之前会自动调用flush()。
			 * 如果你把bw.close(); 也去掉，那么文件里是没有内容的
			 */
			bw.flush();

			while ((s = br.readLine()) != null) {
				System.out.println(s);
			}
			bw.close();
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
