package com.xy6.jvm.io;

import java.io.File;
import java.io.IOException;

public class FileDemo1 {

	public static void main(String[] args) {
		File file = new File("D:" + File.separator + "1.txt");
		if(file.exists()){
			file.delete();
		} else {
			try {
				boolean result = file.createNewFile();
				System.out.println(result);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
