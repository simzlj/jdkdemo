package com.xy6.jvm.staticdemo;

import org.junit.Test;

public class FloatTest {
	
	/**
	 * 在数学中0到1有无数个浮点数；
	 * 而计算机是离散的，所以表示的时候有误差，计算机用精度（小数点后几位来表示正确）
	 */
	@Test
	public void testFloat1(){
		float f1 = 1.2f;
		
		System.out.println(f1 == 1.2);
		System.out.println(f1 == 1.2f);
		System.out.println(f1 - 1.2 == 0);
		System.out.println(f1 - 1.2f == 0);
		System.out.println(f1 - 1.2);
		System.out.println(f1 - 1.2f);
	}

}
