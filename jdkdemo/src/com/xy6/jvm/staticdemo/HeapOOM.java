package com.xy6.jvm.staticdemo;

import java.util.ArrayList;
import java.util.List;

public class HeapOOM {

	static class OOMObject {
	}
	
	public static void main(String[] args){
		List<String[]> list = new ArrayList<String[]>();
		while(true){
			list.add(new String[1000000]);
		}
	}
	
}
