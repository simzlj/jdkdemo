package com.xy6.jvm.staticdemo;

import org.junit.Test;

/**
 * String#Intern用法示例
 * 
 * @author zhang
 * @since 2018-01-14
 */
public class StringIntern1 {
	
	@Test
	public void testIntern1() {
		String str1 = "a";
		String str2 = "b";
		String str3 = "ab";
		String str4 = str1 + str2;
		String str5 = new String("ab");

		System.out.println(str5.equals(str3));
		System.out.println(str5 == str3);
		System.out.println(str5.intern() == str3);
		System.out.println(str5.intern() == str4);
	}

	@Test
	public void testIntern2() {
		String a = new String("ab");
		String b = new String("ab");
		String c = "ab";
		String d = "a" + "b";
		String e = "b";
		String f = "a" + e;

		System.out.println(b.intern() == a);
		System.out.println(b.intern() == c);
		System.out.println(b.intern() == d);
		System.out.println(b.intern() == f);
		System.out.println(b.intern() == a.intern());
	}

	@Test
	public void testIntern3() {
		String a = "abc";
		String b = "abc";
		String c = "a" + "b" + "c";
		String d = "a" + "bc";
		String e = "ab" + "c";

		System.out.println(a == b);
		System.out.println(a == c);
		System.out.println(a == d);
		System.out.println(a == e);
		System.out.println(c == d);
		System.out.println(c == e);
	}

	@Test
	public void testIntern4() {
		String s = new String("1");
		s.intern();
		String s2 = "1";
		System.out.println(s == s2);

		String s3 = new String("2") + new String("3");
		s3.intern();
		String s4 = "23";
		System.out.println(s3 == s4);

		String s5 = new String("4");
		String s6 = s5 + new String("5");
		String s7 = "45";
		System.out.println(s6 == s7);
	}

	@Test
	public void testIntern5() {
		String s = new String("1");
		String s2 = "1";
		s.intern();
		System.out.println(s == s2);

		String s3 = new String("1") + new String("1");
		String s4 = "11";
		s3.intern();
		System.out.println(s3 == s4);
	}

}
