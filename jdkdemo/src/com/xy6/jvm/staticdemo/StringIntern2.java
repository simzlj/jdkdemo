package com.xy6.jvm.staticdemo;

import java.util.Random;

/**
 * 不使用 intern 的代码生成了1000w 个字符串，占用了大约640m 空间。 
 * 使用了 intern 的代码生成了1345个字符串，占用总空间 133k 左右
 * <pre>
 * https://tech.meituan.com/in_depth_understanding_string_intern.html
 * </pre>
 * 
 * @author zhang
 * @since 2018-01-14
 */
public class StringIntern2 {

	static final int MAX = 1000 * 10000;
	static final String[] arr = new String[MAX];
	
	public static void main(String[] args) {
		Integer[] DB_DATA = new Integer[10];
		Random random = new Random(10 * 10000);
		for (int i = 0; i < DB_DATA.length; i++) {
			DB_DATA[i] = random.nextInt();
		}
		long t = System.currentTimeMillis();
		for (int i = 0; i < MAX; i++) {
			arr[i] = new String(String.valueOf(DB_DATA[i % DB_DATA.length]));
			//arr[i] = new String(String.valueOf(DB_DATA[i % DB_DATA.length])).intern();
		}

		System.out.println((System.currentTimeMillis() - t) + "ms");
		System.gc();
		System.out.println("a");
	}

}
