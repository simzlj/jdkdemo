package com.xy6.jvm.lock.deadlock.demo;

public class B1 {
	
	public synchronized void m1(A1 a){
		System.out.println("B1.m1 begin");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		a.m2();
		System.out.println("B1.m1 end");
	}
	
	public synchronized void m2(){
		System.out.println("B1.m2 begin");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("B1.m2 end");
	}
}
