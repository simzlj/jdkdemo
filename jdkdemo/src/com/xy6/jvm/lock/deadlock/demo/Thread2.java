package com.xy6.jvm.lock.deadlock.demo;

public class Thread2 extends Thread {
	
	private A1 a;
	private B1 b;
	
	public Thread2(A1 a, B1 b){
		this.a = a;
		this.b = b;
	}
	
	@Override
	public void run(){
		b.m1(a);
	}
	
}
