package com.xy6.jvm.lock.deadlock.demo;

public class Client1 {

	public static void main(String[] args) throws InterruptedException {
		A1 a = new A1();
		B1 b = new B1();
		int p1 = Double.valueOf(Math.random() * 10).intValue() % 5 + 3;
		int p2 = Double.valueOf(Math.random() * 10).intValue() % 5 + 3;
		System.out.println(p1);
		System.out.println(p2);
		Thread t1 = new Thread1(a, b);
		Thread t2 = new Thread2(a, b);
		// 此处设置优先级无效
		t1.setPriority(p1);
		t1.setPriority(p2);
		t1.start();
		t2.start();

		Thread.currentThread().join();
	}

}
