package com.xy6.jvm.lock.deadlock.demo;

public class A1 {

	public synchronized void m1(B1 b){
		System.out.println("A.m1 begin");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		b.m2();
		System.out.println("A.m1 end");
	}
	
	public synchronized void m2(){
		System.out.println("A.m2 begin");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("A.m2 end");
	}
	
}
