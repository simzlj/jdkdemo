package com.xy6.jvm.lock.deadlock.demo;

public class Thread1 extends Thread {

	private A1 a;
	private B1 b;
	
	public Thread1(A1 a, B1 b){
		this.a = a;
		this.b = b;
	}
	
	@Override
	public void run(){
		a.m1(b);
	}
	
}
