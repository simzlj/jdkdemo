package com.xy6.jvm.tree.catalog;

import java.util.ArrayList;
import java.util.List;

public class NodeTest {

	public static void main(String[] args){
		// 初始化节点列表
		List<Node> nodes = init();
		System.err.println(nodes);
		
		// 遍历节点，查找其子节点
		for(Node elem : nodes){
			for(Node otherElem : nodes){
				if(elem.getCode().equals(otherElem)){
					continue;
				}
				if(elem.getCode().equals(otherElem.getParentCode())){
					elem.getChildren().add(otherElem);
				}
			}
		}
		System.err.println(nodes);
		
		// 遍历节点，找到根节点。根节点包含所有子节点
		List<Node> roots = new ArrayList<>();
		for(Node elem : nodes){
			// 父级编码为空，视为根节点
			if(null == elem.getParentCode()){
				roots.add(elem);
			}
		}
		System.err.println(roots);
	}
	
	/**
	 * 造数据，生成节点列表
	 * 
	 * @return
	 */
	public static List<Node> init(){
		List<Node> nodes = new ArrayList<>();
		Node node;
		
		node = new Node();
		node.setCode("01");
		node.setName("aa");
		node.setParentCode(null);
		nodes.add(node);
		
		node = new Node();
		node.setCode("02");
		node.setName("bb");
		node.setParentCode(null);
		nodes.add(node);
		
		node = new Node();
		node.setCode("0101");
		node.setName("cc");
		node.setParentCode("01");
		nodes.add(node);
		
		node = new Node();
		node.setCode("010101");
		node.setName("dd");
		node.setParentCode("0101");
		nodes.add(node);
		
		node = new Node();
		node.setCode("0201");
		node.setName("dd");
		node.setParentCode("02");
		nodes.add(node);
		
		return nodes;
	}
	
}
