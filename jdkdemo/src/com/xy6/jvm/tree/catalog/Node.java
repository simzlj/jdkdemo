package com.xy6.jvm.tree.catalog;

import java.util.LinkedList;
import java.util.List;

public class Node {

	private String code;
	
	private String name;

	private String parentCode;
	
	private List<Node> children = new LinkedList<>();
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Node> getChildren() {
		return children;
	}

	public void setChildren(List<Node> children) {
		this.children = children;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	@Override
	public String toString() {
		return "Node [code=" + code + ", name=" + name + ", parentCode=" + parentCode + ", children=" + children + "]";
	}

}
