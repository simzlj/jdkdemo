package com.xy6.jvm.reflect;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BillHelper {

	public Map<String, Object> forecast(List<Map<String, Object>> list){
		if(null == list || list.isEmpty()){
			return null;
		}
		for(Map<String, Object> elem : list){
			System.out.println(elem);
		}
		Map<String, Object> map = new HashMap<String, Object>(2);
		map.put("name", "abc");
		map.put("measure", 100);
		return map;
	}
	
}
