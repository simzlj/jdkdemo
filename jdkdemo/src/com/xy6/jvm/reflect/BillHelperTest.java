package com.xy6.jvm.reflect;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;

/**
 * 反射执行
 * com.xy6.jvm.reflect.BillHelper.forecast(List<Map<String, Object>>)
 * 方法
 * 
 * @author zhang
 *
 */
public class BillHelperTest {

	public static void main(String[] args){
		test2();
		test3();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void test2(){
		String className = "com.xy6.jvm.reflect.BillHelper";
		String methodName = "forecast";
		String jsonParam = "[{\"JLDBH\":\"001\",\"DFNY\":\"201706\",\"DL\":100}]";
		try{
			Class clazz = Class.forName(className);
			Method method = clazz.getDeclaredMethod(methodName, new Class[]{List.class});
			
			List<Map<String, Object>> listData = new ArrayList<>();
			// 字符串转json对象
			JSONArray jsonArray = JSON.parseArray(jsonParam);
			jsonArray.size();
			for(int i=0; i<jsonArray.size(); i++){
				Map<String, Object> mapTemp = (Map<String, Object>) jsonArray.getJSONObject(i);
				listData.add(mapTemp);
			}
			System.out.println(listData);
			
			// 执行函数
			Object instance = clazz.newInstance();
			// 对于非静态方法，第一个参数必须为类的实例，而非null !!
			Map<String, Object> result = (Map<String, Object>) method.invoke(instance, listData);
			System.err.println(result);
		} catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public static void test3() {
		try {
			Method method = BillHelper.class.getMethod("forecast", List.class);

			Type[] genericParameterTypes = method.getGenericParameterTypes();

			for (Type type : genericParameterTypes) {
				System.out.println("parameterArgClass = " + type);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
}
