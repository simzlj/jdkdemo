package com.xy6.jvm.volatiles;

public class ReorderMain {

	public static void main(String[] args) {
		test1();
	}

	private static void test1() {
		int num = 10000;
		for (int i = 0; i < num; i++) {
			Thread t = new Thread(new Thread1(), "thread-" + i);
			t.start();
		}
	}

}
