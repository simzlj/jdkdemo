package com.xy6.jvm.thread.threadlocal;

public class Student {

	private int age;

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
}
