package com.xy6.jvm.thread.stopthread;

import java.util.concurrent.TimeUnit;

/**
 * 线程操作示例。
 * <pre>
 * 解决线程安全问题的最佳办法是：不共享可变的数据。
 * 要么共享不可变的数据，要么不共享，将可变数据限制在单个线程中。
 * 当多个线程共享可变数据的时候，每个读或写数据的线程必须执行同步。
 * </pre>
 * 
 * @author zhang
 * @since 2018-05-03
 */
public class StopThread3 {

	/**
	 * volatile修饰符保证任何一个线程在读取该变量时，都读取到最新的值。
	 * 它告诉虚拟机直接在共享内存区域修改变量值，而非在线程的工作内存中修改。
	 */
	private static volatile boolean stopRequested;
	
	public static void main(String[] args) throws InterruptedException{
		Thread backgroundThread = new Thread(new Runnable(){
			@SuppressWarnings("unused")
			@Override
			public void run() {
				int i=0;
				while(!stopRequested){
					i++;
				}
			}
		});
		backgroundThread.start();
		TimeUnit.SECONDS.sleep(1);
		stopRequested = true;
	}
	
}
