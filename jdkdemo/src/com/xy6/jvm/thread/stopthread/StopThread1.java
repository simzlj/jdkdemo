package com.xy6.jvm.thread.stopthread;

import java.util.concurrent.TimeUnit;

/**
 * 线程操作示例。
 * jvm执行时，将行22优化为while(true){i++;}，jvm编译优化技术之一：代码提升 hoisting
 * 程序陷入死循环
 * 
 * @author zhang
 * @since 2018-05-03
 */
public class StopThread1 {

	private static boolean stopRequested;
	
	public static void main(String[] args) throws InterruptedException{
		Thread backgroundThread = new Thread(new Runnable(){
			@SuppressWarnings("unused")
			@Override
			public void run() {
				int i=0;
				while(!stopRequested){
					i++;
				}
			}
		});
		backgroundThread.start();
		TimeUnit.SECONDS.sleep(1);
		stopRequested = true;
	}
	
}
