package com.xy6.jvm.thread.synchronizer;

/**
 * 求和
 * 
 * @author zhang
 * @since 2018-05-04
 */
public class SumThread implements Runnable {

	@Override
	public void run(){
		System.out.println(String.format("%s run begin", Thread.currentThread().getName()));
		long sum = 0;
		for(int i=0; i<10000; i++){
			sum += i;
		}
		System.out.println(String.format("%s %d", Thread.currentThread().getName(), sum));
		System.out.println(String.format("%s run end", Thread.currentThread().getName()));
	}
	
}
