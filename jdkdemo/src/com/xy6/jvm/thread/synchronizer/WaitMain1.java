package com.xy6.jvm.thread.synchronizer;

/**
 * 测试同步块中的代码是否阻塞后续的代码，结论：是的。
 * 
 * @author zhang
 *
 */
public class WaitMain1 {

	public static void main(String[] args) throws InterruptedException {
		WaitMain1 wait = new WaitMain1();
		synchronized (wait){
			System.out.println("a");
			Thread.sleep(3000);
			System.out.println("c");
		}
		System.out.println("b");
	}

}
