package com.xy6.jvm.thread.synchronizer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 并发编程示例
 * 
 * @author zhang
 * @since 2018-05-04
 */
public class ConcurrentTimerTest1 {

	public static void main(String[] args) throws InterruptedException{
		ExecutorService executor = Executors.newFixedThreadPool(3);
		ConcurrentTimer.time(executor, 3, new SumThread());
		executor.shutdown();
	}
	
}
