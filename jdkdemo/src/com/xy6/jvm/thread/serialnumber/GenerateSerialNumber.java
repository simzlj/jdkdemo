package com.xy6.jvm.thread.serialnumber;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 生成唯一序列号，每次获取后序列号加1
 * 
 * @author zhang
 * @since 2018-05-03
 */
public class GenerateSerialNumber {

	/**
	 * 最好的实现方案
	 */
	private static AtomicInteger serialNumber = new AtomicInteger(0);

	public static int generate() {
		return serialNumber.getAndIncrement();
	}

	public static class SerialNumberThread implements Runnable{
		@Override
		public void run() {
			System.out.println(generate());
		}
	}
	
	public static void main(String[] args) throws InterruptedException {
		for(int i=0; i<10000; i++){
			Thread t = new Thread(new SerialNumberThread(), "t" + i);
			t.start();
		}
//		Thread.currentThread().join();
	}

}
