package com.xy6.jvm.thread.interrupt;

/**
 * 测试中断线程时，哪个线程被中断，主线程还是创建的线程
 * 
 * @author zhang
 * @since 2018-05-18
 */
public class MyInterrupt {

	public static void main(String[] args) throws InterruptedException {
		System.out.println(String.format("main: %s", Thread.currentThread().getName()));
		MyThread mythread = new MyThread();
		Thread t = new Thread(mythread, "t1");
		t.start();
		
		Thread.sleep(2000);
		// main中断
//		mythread.test1();
		// t1中断
		mythread.test2();
		
		try {
			Thread.currentThread().join();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
