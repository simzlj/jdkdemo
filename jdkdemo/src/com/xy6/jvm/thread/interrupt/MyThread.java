package com.xy6.jvm.thread.interrupt;

/**
 * @author zhang
 * @since
 */
public class MyThread implements Runnable {
	private MyBean1 m;

	@Override
	public void run() {
		System.out.println(String.format("run: %s", Thread.currentThread().getName()));
		m = new MyBean1();
		m.myinterrupt2();
		System.out.println("run end");
	}
	
	public void test1(){
		m.myinterrupt1();
	}
	
	public void test2(){
		System.out.println("set flag=true");
		m.stopFlag = true;
	}
	
}
