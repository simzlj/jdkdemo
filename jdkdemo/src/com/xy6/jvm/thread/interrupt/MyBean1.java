package com.xy6.jvm.thread.interrupt;

/**
 * @author zhang
 * @since 
 */
public class MyBean1 {
	public volatile boolean stopFlag = false;

	/**
	 * 输出的线程为main
	 */
	public void myinterrupt1(){
		System.out.println(String.format("myinterrupt: %s", Thread.currentThread().getName()));
		Thread.currentThread().interrupt();
	}

	/**
	 * 输出的线程为t1
	 */
	public void myinterrupt2(){
		System.out.println(String.format("myinterrupt: %s", Thread.currentThread().getName()));
		while(true){
			try {
				System.out.println("stopFlag:" + stopFlag);
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if(stopFlag){
				try{
					Thread.currentThread().interrupt();
				} catch(Exception e){
					e.printStackTrace();
				}
				break;
			}
		}
	}
}
