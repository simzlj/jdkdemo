package com.xy6.jvm.thread.observer;

import java.util.HashSet;

/**
 * 多线程示例，一个可观察的集合。观察到集合中元素变化时，触发事件。
 * 
 * 结论：不要在同步区域内部调用外来方法
 * 
 * @author zhang
 * @since 2018-05-03
 */
public class Test1 {

	public static void main(String[] args) {
		ObservableSet<Integer> set = new ObservableSet<Integer>(new HashSet<Integer>());
		
		// 观察到目标变化后，发生的事件。可添加多个事件
		set.addObserver(new SetObserver<Integer>(){
			@Override
			public void added(ObservableSet<Integer> s, Integer element) {
				System.out.println(element);
				if(element == 6){
					s.removeObserver(this);
				}
			}
		});
		
		for(int i=0; i<10; i++){
			set.add(i);
		}
		
	}

}
