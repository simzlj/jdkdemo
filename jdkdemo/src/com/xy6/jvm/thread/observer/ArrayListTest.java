package com.xy6.jvm.thread.observer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ArrayListTest {

	public static void main(String[] args) {
		List<Integer> elems = new ArrayList<>(5);
		elems.add(1);
		elems.add(2);
		elems.add(3);
		Iterator<Integer> iter = elems.iterator();
		while(iter.hasNext()){
			if(iter.next() == 2){
				iter.remove();
			}
		}
		System.out.println(elems);
	}

}
