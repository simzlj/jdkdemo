package com.xy6.jvm.thread.observer;


public interface SetObserver<E> {

	/**
	 * invoked when an element is added to the observable set
	 * 
	 * @param set
	 * @param element
	 */
	void added(ObservableSet<E> set, E element);
	
}
