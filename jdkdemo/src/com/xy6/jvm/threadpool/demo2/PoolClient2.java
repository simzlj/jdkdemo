package com.xy6.jvm.threadpool.demo2;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 使用线程池实现生产者消费者
 * 
 * @author zhang
 * @since 2018-04-30
 */
public class PoolClient2 {

	public static void main(String[] args) {
		ThreadPoolExecutor pool = new ThreadPoolExecutor(5, 10, 200, TimeUnit.MILLISECONDS, 
				new LinkedBlockingQueue<Runnable>());
		
		LinkedBlockingQueue<Integer> queue = new LinkedBlockingQueue<Integer>();
		ProducterTask producter = new ProducterTask(queue);
		ConsumerTask consumer = new ConsumerTask(queue);
		
		pool.execute(producter);
		pool.execute(consumer);
		
		pool.shutdown();
	}

}
