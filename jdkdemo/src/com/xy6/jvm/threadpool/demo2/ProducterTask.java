package com.xy6.jvm.threadpool.demo2;

import java.util.concurrent.LinkedBlockingQueue;

public class ProducterTask implements Runnable {

	private LinkedBlockingQueue<Integer> queue;
	
	public ProducterTask(LinkedBlockingQueue<Integer> queue){
		this.queue = queue;
	}
	
	@Override
	public void run() {
		try {
			for(int i=0; i<50; i++){
				int elem = Double.valueOf(Math.random() * 100).intValue(); 
				System.out.println("product a elem: " + elem);
				queue.put(elem);
				Thread.sleep(1000);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
