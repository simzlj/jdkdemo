package com.xy6.jvm.threadpool.demo2;

import java.util.concurrent.LinkedBlockingQueue;

public class ConsumerTask implements Runnable {

	private LinkedBlockingQueue<Integer> queue;
	
	public ConsumerTask(LinkedBlockingQueue<Integer> queue){
		this.queue = queue;
	}
	
	@Override
	public void run(){
		try {
			for(int i=0; i<50; i++){
				System.out.println("consum a elem: " + queue.poll());
				Thread.sleep(1000);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
