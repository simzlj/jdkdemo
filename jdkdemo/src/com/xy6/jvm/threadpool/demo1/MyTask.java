package com.xy6.jvm.threadpool.demo1;

/**
 * 一个线程任务实现类，模拟业务系统的任务
 * 
 * @author zhang
 * @since 2018-04-30
 */
public class MyTask implements Runnable {

	private int taskNum;
	
	public MyTask(int num){
		this.taskNum = num;
	}
	
	@Override
	public void run() {
		System.out.println("正在执行task " + taskNum);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("task " + taskNum + "执行完毕");
	}

}
