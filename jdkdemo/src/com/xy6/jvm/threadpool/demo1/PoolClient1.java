package com.xy6.jvm.threadpool.demo1;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 线程池使用示例。初始化一个固定大小的线程池，向池中添加一定数量的任务，观测线程池变化状态
 * 
 * @author zhang
 * @since 2018-04-30
 */
public class PoolClient1 {

	public static void main(String[] args) {
		ThreadPoolExecutor executor = new ThreadPoolExecutor(5, 10, 200, TimeUnit.MILLISECONDS,
				new ArrayBlockingQueue<Runnable>(5));
		for(int i=0; i<15; i++){
			MyTask myTask = new MyTask(i);
			executor.execute(myTask);
			System.out.println(String.format("线程池中线程数据：%d，队列中等待执行的数据：%d，已执行完别的任务数据：%d", executor.getPoolSize(),
					executor.getQueue().size(), executor.getCompletedTaskCount()));
		}
		System.out.println("hello world");
		executor.shutdown();
	}

}
