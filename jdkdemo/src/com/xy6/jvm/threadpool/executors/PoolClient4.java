package com.xy6.jvm.threadpool.executors;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * 线程池工具类Executors用法示例，一般通过它来创建线程池，而非使用ThreadPoolExecutor
 * <pre>
 * 一般需要根据任务的类型来配置线程池大小：
 * 如果是CPU密集型任务，就需要尽量压榨CPU，参考值可以设为 NCPU+1
 * 如果是IO密集型任务，参考值可以设置为2*NCPU
 * </pre>
 * 
 * @author zhang
 * @since 2018-04-30
 */
public class PoolClient4 {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		ExecutorService pool = Executors.newCachedThreadPool();
		for (int i = 0; i < 1; i++) {
			Future future = pool.submit(new Runnable() {
				@Override
				public void run() {
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			});
			if(future.get() == null){
				System.out.println("done");
			}
		}
		
		System.out.println("hello world");
		pool.shutdown();
	}

}
