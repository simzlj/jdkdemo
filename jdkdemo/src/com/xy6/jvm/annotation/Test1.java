package com.xy6.jvm.annotation;

/**
 * 什么是注解？
 * 用一个词就可以描述注解，那就是元数据，即一种描述数据的数据。所以，可以说注解就是源代码的元数据。
 * Annotation是一种应用于类、方法、参数、变量、构造器及包声明中的特殊修饰符。
 * 它是一种由JSR-175(Java Specification Requests，java规范提案)标准选择用来描述元数据的一种工具。
 * Annotations仅仅是元数据，和业务逻辑无关。
 * 
 * 为什么要引入注解？
 * 使用Annotation之前，XML被广泛的应用于描述元数据。
 * 不知何时开始一些应用开发人员和架构师发现XML的维护越来越糟糕了。
 * 他们希望使用一些和代码紧耦合的东西，而不是像XML那样和代码是松耦合的(在某些情况下甚至是完全分离的)代码描述。
 * 另一个很重要的因素是Annotation定义了一种标准的描述元数据的方式。
 * 在这之前，开发人员通常使用他们自己的方式定义元数据。例如，使用标记interfaces，注释，transient关键字等等。
 * 每个程序员按照自己的方式定义元数据，而不像Annotation这种标准的方式。
 * 
 * java.lang.annotation提供了四种元注解
 * @Documented –注解是否将包含在JavaDoc中
 * @Retention –什么时候使用该注解
 * @Target? –注解用于什么地方
 * @Inherited – 是否允许子类继承该注解
 * 
 * http://www.importnew.com/10294.html
 * 注解的原理 ：
 * https://www.cnblogs.com/acm-bingzi/p/javaAnnotation.html
 * 
 * @author zhang
 * @since 2018-05-05
 */
public class Test1 {

	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// 不管是否有override，程序始终能够正常输出
	// @Override告诉编译器这个方法是一个重写方法(描述方法的元数据)，如果父类中不存在该方法，
	// 编译器便会报错，提示该方法没有重写父类中的方法。
	@Override
	public String toString() {
		return "Test1 [name=" + name + "]";
	}

	public static void main(String[] args) {
		Test1 t1 = new Test1();
		t1.setName("hello");
		System.out.println(t1.toString());
	}

}
