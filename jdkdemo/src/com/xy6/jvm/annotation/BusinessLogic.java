package com.xy6.jvm.annotation;

import com.xy6.jvm.annotation.anno.Author;
import com.xy6.jvm.annotation.anno.Todo;

public class BusinessLogic {

	@Todo(priority=Todo.Priority.MEDIUM, author="zhang", status=Todo.Status.NOT_STARTED)
	public void incompleteMethod1(){
		// some business logic is written 
		// but it's not complete yet
	}
	
	@Author("Yashwant")
	public void someMethod2(){
	}
	
}
