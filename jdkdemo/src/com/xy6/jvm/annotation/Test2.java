package com.xy6.jvm.annotation;

import java.lang.reflect.Method;

import com.xy6.jvm.annotation.anno.Todo;

/**
 * 使用自定义注解的示例
 * 
 * @author zhang
 * @since 2018-05-05
 */
public class Test2 {

	public static void main(String[] args) {
		Class<BusinessLogic> bizLogicClass = BusinessLogic.class;
		// 反射获取方法的注解
		for (Method method : bizLogicClass.getMethods()) {
			Todo todoAnno = (Todo) method.getAnnotation(Todo.class);
			if (null == todoAnno) {
				continue;
			}
			System.out.println(String.format("method name: %s", method.getName()));
			System.out.println(String.format("author: %s", todoAnno.author()));
			System.out.println(String.format("priority: %s", todoAnno.priority()));
			System.out.println(String.format("status: %s", todoAnno.status()));
		}
	}

}
