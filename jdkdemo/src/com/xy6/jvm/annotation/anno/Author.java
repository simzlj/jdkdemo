package com.xy6.jvm.annotation.anno;

public @interface Author {

	String value();
	
}
