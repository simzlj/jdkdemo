package com.xy6.concurrencyart.chapter03;

/**
 * final示例
 * 
 * @author zhang
 * @since 2018-05-23
 */
public class FinalReferenceExample {

	// final是引用类型
	final int[] intArray;
	static FinalReferenceExample obj;
	public FinalReferenceExample(){
		intArray = new int[1];
		intArray[0] = 1;
	}
	// 写线程A执行
	public static void writeOne(){
		obj = new FinalReferenceExample();
	}
	// 写线程B执行
	public static void writerTwo(){
		obj.intArray[0] = 2;
	}
	// 写线程C执行
	public static void reader(){
		if(obj != null){
			int temp1 = obj.intArray[0];
			System.out.println(temp1);
		}
	}
	
	public static void main(String[] args) throws InterruptedException {
		for(int i=0; i<1000; i++){
			Thread t1 = new Thread(new Runnable(){
				@Override
				public void run(){
					FinalReferenceExample.writeOne();
				}
			});
			Thread t2 = new Thread(new Runnable(){
				@Override
				public void run(){
					FinalReferenceExample.writerTwo();
				}
			});
			Thread t3 = new Thread(new Runnable(){
				@Override
				public void run(){
					FinalReferenceExample.reader();
				}
			});
			t1.start();
			t1.join();
			t2.start();
			t2.join();
			t3.start();
			t3.join();
		}
	}

}
