package com.xy6.concurrencyart.chapter03;

import java.util.concurrent.locks.ReentrantLock;

/**
 * ReentrantLock示例
 * 
 * @author zhang
 * @since 2018-05-23
 */
public class ReentrantLockExample {

	int a = 0;
	ReentrantLock lock = new ReentrantLock();
	public void writer(){
		// 获取锁
		lock.lock(); 
		try{
			a++;
		} finally{
			// 释放锁
			lock.unlock();
		}
	}
	public void reader(){
		// 获取锁
		lock.lock();
		try{
			int i = a;
			System.out.println(i);
		} finally{
			// 释放锁
			lock.unlock();
		}
	}
	
	public static void main(String[] args) {
	}

}
