package com.xy6.deepjvm.chapter04;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;

/**
 * 多线程示例
 * 
 * @author zhang
 * @since 2018-05-25
 */
public class MultiThread {

	/**
	 * 
[5]Attach Listener
[4]Signal Dispatcher 分发处理发送给jvm信号的线程
[3]Finalizer 调用对象Finalizer方法的线程
[2]Reference Handler 清除reference的线程
[1]main main线程，用户程序入口
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// 获取java线程管理mxbean
		ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
		// 不需要获取同步的monitor和synchronizer信息，仅获取线程和线程堆栈信息
		ThreadInfo[] threadInfos = threadMXBean.dumpAllThreads(false, false);
		// 遍历线程信息，仅打印线程ID和线程名称
		for(ThreadInfo threadInfo : threadInfos){
			System.out.println(String.format("[%s]%s", threadInfo.getThreadId(), threadInfo.getThreadName()));
		}
	}
	
}
