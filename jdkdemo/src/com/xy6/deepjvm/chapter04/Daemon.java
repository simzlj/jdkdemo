package com.xy6.deepjvm.chapter04;

import java.util.concurrent.TimeUnit;

/**
 * Daemon线程示例
 * 被用作完成支持性工作
 * 
 * @author zhang
 * @since 2018-05-25
 */
public class Daemon {

	public static void main(String[] args) {
		Thread t = new Thread(new DaemonRunner(), "daemon1");
		t.setDaemon(true);
		t.start();
	}

	static class DaemonRunner implements Runnable {
		@Override
		public void run() {
			try {
				TimeUnit.SECONDS.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				System.out.println("finally run");
			}
		}
	}

}
