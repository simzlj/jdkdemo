package com.xy6.deepjvm.chapter04;

import java.util.concurrent.TimeUnit;

/**
 * @author zhang
 * @since 
 */
public class SleepUtils {

	public static void sleep(long seconds){
		try {
			TimeUnit.SECONDS.sleep(seconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void second(long seconds){
		sleep(seconds);
	}
	
}
