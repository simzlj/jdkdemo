package com.xy6.deepjvm.chapter03;

/**
 * 引用计数法gc示例
 * 
 * @author zhang
 * @since 2018-05-23
 */
public class ReferenceCountingGC {
	
	public Object instance = null;
	private static final int _1MB = 1024 * 1024;
	/** 占点内存，以便能在GC日志中看清楚是否被回收过 */
	private byte[] byteSize = new byte[2 * _1MB];

	public static void testGC(){
		ReferenceCountingGC obj1 = new ReferenceCountingGC();
		ReferenceCountingGC obj2 = new ReferenceCountingGC();
		obj1.instance = obj2;
		obj2.instance = obj1;
		obj1 = null;
		obj2 = null;
	}
	
	public static void main(String[] args) {
		testGC();
		
		// 假设发生GC，obj1,obj2能否被回收？
		System.gc();
	}

}
