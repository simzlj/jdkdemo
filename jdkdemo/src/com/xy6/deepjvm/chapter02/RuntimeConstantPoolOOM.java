package com.xy6.deepjvm.chapter02;

import java.util.ArrayList;
import java.util.List;

/**
 * 运行时常量池导致的内存溢出异常
 * 
 * @author zhang
 * @since 2018-05-12
 */
public class RuntimeConstantPoolOOM {

	public static void main(String[] args) {
		// 使用list保持常量池引用，避免full gc回收常量池行为
		List<String> list = new ArrayList<String>();
		// 10M的PermSize在integer范围内足够产生oom
		int i = 0;
		while (true) {
			list.add(String.valueOf(i++).intern());
		}
	}

}
