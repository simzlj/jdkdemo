package com.xy6.deepjvm.chapter02;

import java.lang.reflect.Method;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

/**
 * 借助cglib使方法区出现内存溢出异常
 * 
 * @author zhang
 * @since 2018-05-21
 */
public class JavaMethodAreaOOM {

	public static void main(String[] args) {
		while (true) {
			Enhancer enh = new Enhancer();
			enh.setSuperclass(OOMObject.class);
			enh.setUseCache(false);
			enh.setCallback(new MethodInterceptor() {
				@Override
				public Object intercept(Object obj, Method m1, Object[] args, MethodProxy proxy) throws Throwable {
					return proxy.invokeSuper(obj, args);
				}
			});
			enh.create();
		}
	}

	static class OOMObject {
	}

}
