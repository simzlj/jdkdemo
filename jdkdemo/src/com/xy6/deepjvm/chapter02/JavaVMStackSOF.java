package com.xy6.deepjvm.chapter02;

/**
 * 虚拟机栈和本地方法栈OOM测试
 * 测试栈上抛出outofmemoryerror, stackoverflowerror异常
 * VM Args：-Xss128k
 * 
 * @author zhang
 * @since 2018-05-17
 */
public class JavaVMStackSOF {

	private int stackLength = 0;

	private void stackLeak() {
		stackLength++;
		stackLeak();
	}

	public static void main(String[] args) throws Throwable {
		JavaVMStackSOF oom = new JavaVMStackSOF();
		try {
			oom.stackLeak();
		} catch (Throwable e) {
			System.out.println(String.format("stack length: %d", oom.stackLength));
			throw e;
		}
	}

}
