package com.xy6.deepjvm.chapter02;

/**
 * 创建线程导致内存溢出异常
 * 运行示例可能导致操作系统卡死，慎重
 * 
 * @author zhang
 * @since 2018-05-17
 */
public class JavaVMStackOOM {

	private void dontStop() {
		while (true) {
		}
	}

	private void stackLeakByThread() {
		int i = 0;
		while (true) {
			Thread t = new Thread(new Runnable() {
				@Override
				public void run() {
					dontStop();
				}
			}, String.valueOf(i++));
			t.start();
		}
	}

	public static void main(String[] args) throws Throwable {
		JavaVMStackOOM oom = new JavaVMStackOOM();
		try{
			oom.stackLeakByThread();
		} catch(Throwable e){
			e.printStackTrace();
			throw e;
		}
	}

}
