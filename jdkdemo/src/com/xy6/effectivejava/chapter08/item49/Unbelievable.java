package com.xy6.effectivejava.chapter08.item49;

public class Unbelievable {
	
	static Integer i;

	public static void main(String[] args) {
		// 装箱基本类型自动拆箱，此时导致报空指针
		if(i == 42){
			System.out.println("unbelievable");
		}
	}

}
