package com.xy6.effectivejava.chapter07.item41;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CollectionClassifier {

	public static String classify(Set<?> s) {
		return "set";
	}

	public static String classify(List<?> lst) {
		return "list";
	}

	public static String classify(Collection<?> c) {
		return "unknown collection";
	}

	public static String classify2(Collection<?> c) {
		return c instanceof Set ? "set" : (c instanceof List ? "list" : "unknown");
	}

	public static void main(String[] args) {
		Collection<?>[] collections = { new HashSet<String>(), new ArrayList<BigInteger>(),
				new HashMap<String, String>().values() };
		// true，数组元素都为Collection类型。泛型
		System.out.println(collections[0] instanceof Collection);

		// 3 times: unknown collection
		for (Collection<?> c : collections) {
			System.out.println(classify(c));
		}

		for (Collection<?> c : collections) {
			System.out.println(classify2(c));
		}
	}

}
