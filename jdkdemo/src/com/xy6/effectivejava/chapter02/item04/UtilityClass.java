package com.xy6.effectivejava.chapter02.item04;

/**
 * 不可实例化的工具类
 * 
 * @author zhang
 *
 */
public class UtilityClass {

	/**
	 * 屏蔽默认的构造函数，以禁止实例化
	 */
	private UtilityClass(){
		throw new AssertionError();
	}
	
	public static int sum(int a, int b){
		return a+b;
	}
	
}
