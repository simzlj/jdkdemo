package com.xy6.effectivejava.chapter02.item02;

public class Student {

	private String no;

	private String name;

	private Integer age;

	private Integer height;

	public Student() {
	}

	public Student no(String no) {
		this.no = no;
		return this;
	}

	public Student name(String name) {
		this.name = name;
		return this;
	}

	public Student age(Integer age) {
		this.age = age;
		return this;
	}

	public Student height(Integer height) {
		this.height = height;
		return this;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	@Override
	public String toString() {
		return "Student [no=" + no + ", name=" + name + ", age=" + age + ", height=" + height + "]";
	}

	public static void main(String[] args) {
		Student s = new Student().no("001").name("zhang").age(26).height(177);
		System.out.println(s);
	}

}
