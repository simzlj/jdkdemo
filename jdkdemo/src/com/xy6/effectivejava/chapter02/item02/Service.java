package com.xy6.effectivejava.chapter02.item02;

/**
 * service interface
 * 
 * @author zhang
 *
 */
public interface Service {

	// service-specific methods go here
	
}
