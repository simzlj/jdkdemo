package com.xy6.effectivejava.chapter02.item02;

/**
 * service provier interface
 * 
 * @author zhang
 *
 */
public interface Provider {

	Service newService();
	
}
