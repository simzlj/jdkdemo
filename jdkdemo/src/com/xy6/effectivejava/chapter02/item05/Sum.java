package com.xy6.effectivejava.chapter02.item05;

public class Sum {

	private static void calc(){
		// Long -> 7.5s
		// long -> 1.6s
		long sum = 0L;
		for (long i = 0; i < Integer.MAX_VALUE; i++) {
			sum += i;
		}
		System.out.println(sum);
	}
	
	public static void main(String[] args) {
		long time1 = System.nanoTime();
		calc();
		long time2 = System.nanoTime();
		System.out.println(String.format("cost time %d ms", (time2 - time1) / 1000000));
	}

}
