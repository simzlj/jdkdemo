package com.xy6.effectivejava.chapter04.item26;

import java.util.HashSet;
import java.util.Set;

public class MyUtils {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Set union(Set s1, Set s2){
		Set result = new HashSet(s1);
		result.addAll(s2);
		return result;
	}
	
	public static <E> Set<E> union2(Set<E> s1, Set<E> s2){
		Set<E> result = new HashSet<>(s1);
		result.addAll(s2);
		return result;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args){
		Set s1 = new HashSet();
		s1.add(1);
		Set s2 = new HashSet();
		s2.add(2);
		Set s3 = MyUtils.union(s1, s2);
		System.out.println(s3);
		
		Set<Integer> s21 = new HashSet<>();
		s21.add(1);
		Set<Integer> s22 = new HashSet<>();
		s22.add(2);
		Set<Integer> s23 = MyUtils.union2(s21, s22);
		System.out.println(s23);
	}
	
}
