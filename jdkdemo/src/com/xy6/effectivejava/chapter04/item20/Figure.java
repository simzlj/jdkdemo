package com.xy6.effectivejava.chapter04.item20;

public abstract class Figure {

	abstract double area();
	
}
