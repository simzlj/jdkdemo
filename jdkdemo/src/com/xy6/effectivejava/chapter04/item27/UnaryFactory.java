package com.xy6.effectivejava.chapter04.item27;

public class UnaryFactory {

	private static UnaryFunction<Object> IDENTIFY_FUNCTION = new UnaryFunction<Object>() {
		{
			System.out.println("create a instance");
			
		}
		public Object apply(Object arg) {
			return arg;
		}
	};

	@SuppressWarnings("unchecked")
	public static <T> UnaryFunction<T> identifyFunction() {
		return (UnaryFunction<T>) IDENTIFY_FUNCTION;
	}

	public static void main(String[] args) {
		String[] strings = { "hello", "world" };
		UnaryFunction<String> sameString = identifyFunction();
		for (String s : strings) {
			System.out.println(sameString.apply(s));
		}

		Number[] numbers = { 1, 2, 3L };
		UnaryFunction<Number> sameNum = identifyFunction();
		for(Number num : numbers){
			System.out.println(sameNum.apply(num));
		}
	}

}
