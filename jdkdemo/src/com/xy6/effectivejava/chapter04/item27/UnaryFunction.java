package com.xy6.effectivejava.chapter04.item27;

public interface UnaryFunction<T> {

	T apply(T arg);
	
}
