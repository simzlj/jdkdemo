package com.xy6.effectivejava.chapter04.item27;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * 泛型用法终极示例demo
 * 
 * @author zhang
 *
 */
public class MaxUtils {

	/**
	 * 查找一个集合的最大元素，并返回
	 * 
	 * @param list
	 * @return
	 */
	public static <T extends Comparable<? super T>> T max(
			List<? extends T> list){
		Iterator<? extends T> i = list.iterator();
		T result = i.next();
		while(i.hasNext()){
			T t = i.next();
			if(t.compareTo(result) > 0){
				result = t;
			}
		}
		return result;
	}
	
	public static void main(String[] args) {
		List<String> str = Arrays.asList(new String[]{"bb", "aa", "cc"});
		String elem = max(str);
		System.out.println(elem);
	}
	
}
