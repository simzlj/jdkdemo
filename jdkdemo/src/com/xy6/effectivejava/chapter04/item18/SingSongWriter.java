package com.xy6.effectivejava.chapter04.item18;

public interface SingSongWriter extends Singer, SongWriter {

	void strum();
	
	void actSensitive();
	
}
