package com.xy6.effectivejava.chapter04.item18;

import java.util.AbstractList;
import java.util.List;

public class MyList {

	public static List<Integer> intArrayAsList(final int[] a){
		if(null == a){
			throw new NullPointerException();
		}
		
		return new AbstractList<Integer>(){
			public Integer get(int i){
				return a[i];
			}
			
			@Override
			public Integer set(int i, Integer val){
				int oldVal = a[i];
				a[i] = val;
				return oldVal;
			}
			
			public int size(){
				return a.length;
			}
		};
	}
	
	public static void main(String[] args){
		int[] a = new int[3];
		List<Integer> list = MyList.intArrayAsList(a);
		list.get(0);
		list.set(1, 2);
		System.out.println(list.size());
		System.out.println(list);
	}
	
}
