package com.xy6.effectivejava.chapter05.item34;

public interface Operation {

	double apply(double x, double y);
	
}
