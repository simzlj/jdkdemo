package com.xy6.effectivejava.chapter05.item34;

public class Test1 {

	public static void main(String[] args) {
		double x = 1d;
		double y = 2d;
		test(BasicOperation.class, x, y);
	}
	
	private static <T extends Enum<T> & Operation> void test(Class<T> opSet, double x, double y){
		for(Operation op : opSet.getEnumConstants()){
			System.out.printf("%f %s %f = %f %n", x, op, y, op.apply(x, y));
		}
	}

}
