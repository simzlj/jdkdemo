package com.xy6.effectivejava.chapter05.item30;

import java.util.EnumSet;
import java.util.Set;

/**
 * EnumSet - a modern replacement of bit fields
 * 
 * @author zhang
 * @since 2018-05-11
 */
public class Text {

	public enum Style {
		BOLD, ITALIC, UNDERLINE
	}

	/**
	 * any set could be passed in, but EnumSet is clearly best
	 * 
	 * @param styles
	 */
	public void applyStyles(Set<Style> styles) {
		System.out.println(styles);
	}

	public static void main(String[] args) {
		Text text = new Text();
		text.applyStyles(EnumSet.of(Style.BOLD, Style.ITALIC));
	}

}
