package com.xy6.effectivejava.chapter05.item33;

import java.util.EnumMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * EnumMap用法示例
 * 
 * @author zhang
 *
 */
public class Herb {
	public enum Type {
		/** 一年生 */
		ANNUAL,

		/** 两年生 */
		BIENNIAL,

		/** 多年生 */
		PERENNIAL
	}

	private final String name;

	private final Type type;

	private Herb(String name, Type type) {
		this.name = name;
		this.type = type;
	}

	@Override
	public String toString() {
		return name;
	}

	public static void main(String[] args) {
		Herb[] garden = new Herb[3];
		garden[0] = new Herb("aa", Type.ANNUAL);
		garden[1] = new Herb("bb", Type.BIENNIAL);
		garden[2] = new Herb("cc", Type.BIENNIAL);

		// using an EnumMap to associate data with an enum
		Map<Herb.Type, Set<Herb>> herbsByType = new EnumMap<Herb.Type, Set<Herb>>(Herb.Type.class);
		for (Herb.Type t : Herb.Type.values()) {
			herbsByType.put(t, new HashSet<Herb>());
		}
		for (Herb h : garden) {
			herbsByType.get(h.type).add(h);
		}
		System.out.println(herbsByType);
	}

}
