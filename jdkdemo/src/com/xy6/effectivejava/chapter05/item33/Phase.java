package com.xy6.effectivejava.chapter05.item33;

import java.util.EnumMap;
import java.util.Map;

public enum Phase {

	SOLID, LIQUID, GAS;

	/**
	 * 转换
	 */
	public enum Transition {
		/** 熔化 */
		MELT(SOLID, LIQUID),
		/** 凝固 */
		FREEZE(LIQUID, SOLID),
		/** 蒸发 */
		BOIL(LIQUID, GAS),
		/** 凝结 */
		CONDENSE(GAS, LIQUID),
		/** 升华 */
		SUBLIME(SOLID, GAS),
		/** 沉淀 */
		DEPOSIT(GAS, SOLID);

		private final Phase src;
		private final Phase dst;

		Transition(Phase src, Phase dst) {
			this.src = src;
			this.dst = dst;
		}

		private static final Map<Phase, Map<Phase, Transition>> m = new EnumMap<Phase, Map<Phase, Transition>>(
				Phase.class);
		static {
			for (Phase p : Phase.values()) {
				m.put(p, new EnumMap<Phase, Transition>(Phase.class));
			}
			for (Transition trans : Transition.values()) {
				m.get(trans.src).put(trans.dst, trans);
			}
		}

		public static Transition from(Phase src, Phase dst) {
			return m.get(src).get(dst);
		}
	}

}
